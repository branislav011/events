// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
      apiKey: "AIzaSyAeM6ViTLoUMOtRNi7PQdEiU5n_eBK18n8",
      authDomain: "events-af01d.firebaseapp.com",
      projectId: "events-af01d",
      storageBucket: "events-af01d.appspot.com",
      messagingSenderId: "171603223774",
      appId: "1:171603223774:web:38aef448a9447154a5ce4f"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
