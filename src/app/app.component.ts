import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'events';

  /**
   * @todo
   * Angular
   * Angular Material
   * Flex Layout
   * Firebase
   * Tests
   * Docker
   */

  /**
   * login
   * register
   * home
   * events
   * about
   */

  /**
   * - shared
   *    - components
   *      - layout
   *        - header
   *        - footer
   *        - menu
   * - modules
   *   - users
   *     -components
   *        - home
   *        - events
   *        - about
   *     - services
   *     - models
   *   - auth
   *      -components
   *      - services
   *   - material
   */

  //https://stackoverflow.com/questions/52933476/angular-project-structure-best-practice
}
