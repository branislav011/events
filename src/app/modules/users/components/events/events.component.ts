import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventsService } from 'src/app/shared/services/events/events.service';
import { AddEventComponent } from '../../modals/add-event/add-event.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  subscription: Subscription = new Subscription();

  constructor(private eventService: EventsService, private dialog: MatDialog) { }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * add event modal
   */
  showAddEventDialog(): void {
    const dialogRef = this.dialog.open(AddEventComponent);

    this.subscription = dialogRef.afterClosed().subscribe(result => {
      if (result && result.data) {
        //save
        this.addEvent(result.data);
      }
    });
  }

  /**
   * save new event to firebase
   */
  addEvent(data: object): void {
    this.eventService.addEvent(data);
  }

}
