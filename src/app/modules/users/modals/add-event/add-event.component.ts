import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styles: [
  ]
})
export class AddEventComponent implements OnInit {
  /**
   * form fields - add events
   * set default values
   */
  name: string = "";
  desc: string = "";
  date: string = "";

  constructor(public dialogRef: MatDialogRef<AddEventComponent>) { }

  ngOnInit(): void {
    
  }

  save(): void|boolean {
    if (!this.name || !this.desc || !this.date) {
      return false;
    }

    let date = new Date(this.date);

    //send data to component
    this.dialogRef.close({
      data: {
        name: this.name,
        desc: this.desc,
        date: date,
        type: "upcoming",
      }
    });

    //reset fields
    this.name = "";
    this.desc = "";
    this.date = "";
  }

}
