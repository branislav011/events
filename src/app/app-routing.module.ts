import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './modules/auth/components/login/login.component';
import { RegisterComponent } from './modules/auth/components/register/register.component';
import { GuardService } from './modules/auth/services/auth/guard.service';
import { AboutComponent } from './modules/users/components/about/about.component';
import { EventsComponent } from './modules/users/components/events/events.component';
import { HomeComponent } from './modules/users/components/home/home.component';

const routes: Routes = [
  {path: "", component: HomeComponent, canActivate: [GuardService]},
  {path: "events", component: EventsComponent, canActivate: [GuardService]},
  {path: "about", component: AboutComponent, canActivate: [GuardService]},
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
