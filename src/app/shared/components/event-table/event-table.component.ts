import { Component, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { EventsService } from 'src/app/shared/services/events/events.service';

@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.css']
})
export class EventTableComponent implements OnInit {
  /**
   * upcoming | previos events
   */
  @Input() type: string = "";

  /**
   * set paginator and sort vars
   */
  @ViewChild(MatSort, { static: false }) sort!: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator!: MatPaginator;

  /**
   * table paginator default values
   */
  perPage: number = 5;
  options: number[] = [1, 5, 10, 25, 100];
  tableLength: number = 0;

  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  displayedColumns: string[] = [];
  subscription = new Subscription();

  constructor(private eventService: EventsService) {
    
  }

  ngOnInit(): void {
    let type = this.type;

    /**
     * get table data and columns
     * set paginator length
     */
    this.subscription = this.eventService.getEvents(type).subscribe(data => {
      this.dataSource.data = data;
      this.tableLength = data.length;
    });

    this.displayedColumns = this.eventService.getTableColumns();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  updateEvent(id: string): void {
    let type = this.type;
    let newType = "upcoming";

    if (type == "upcoming") {
      newType = "previous";
    }

    this.eventService.updateEvent(id, newType);
  }

  deleteEvent(id: string): void {
    this.eventService.deleteEvent(id);
  }

  filterEvents(event: any): void {
    let value: string = event.target.value;
    value = value.trim().toLowerCase();

    this.dataSource.filter = value;
  }
}
