export interface Event {
    id: string;
    name: string;
    desc?: string;
    date: Date;
    type: "upcoming" | "previous";
}
