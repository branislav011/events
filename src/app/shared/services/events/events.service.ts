import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from "rxjs/operators";
import { Event } from '../../models/event/event';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  eventList$: Observable<Event[]>;
  columns: string[] = [];

  constructor(private firestore: AngularFirestore) {
    this.eventList$ = firestore.collection<Event>('events').valueChanges({idField: 'eventID'});

    this.columns = ["name", "desc", "date", "restore", "delete"];
  }

  /**
   * Filter events by type
   * upcoming | previous
   */
  allEventsByType(type: string): Observable<Event[]> {
    return this.eventList$
    .pipe(
        map(events => events.filter(event => event.type === type))
    );
  }

  getEvents(type: string): Observable<Event[]> {
    return this.allEventsByType(type);
  }

  /**
   * Columns for previous and upcoming events - tables
   */
  getTableColumns(): string[] {
    return this.columns;
  }

  /**
   * store to firebase
   * new record
   */
  addEvent(data: object): void {
    let event = data as Event[];

    this.firestore.collection('events').add(event);
  }

  /**
   * update firebase record
   */
  updateEvent(id: string, type: string): void {
    this.firestore.collection("events").doc(id).update({
      type,
    });
  }

  /**
   * delete firebase record
   */
  deleteEvent(id: string): void {
    this.firestore.collection("events").doc(id).delete();
  }
}
